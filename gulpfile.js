import gulp from "gulp";
const { parallel, series, src, dest, watch } = gulp;

import rename from "gulp-rename";

import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
import autoprefixer from "gulp-autoprefixer";
import cleanCss from "gulp-clean-css";

import uglify from "gulp-uglify";

import imagemin from "gulp-imagemin";

import browserSync from "browser-sync";
const bsServer = browserSync.create();

function serve() {
  bsServer.init({
    server: {
      baseDir: "./",
      browser: "firefox",
    },
  });
}

import { deleteAsync as del } from "del";

function clear() {
    return del('./dist');  
 }

function images() {
  return src("./src/img/**/*.{jpg,jpeg,png,svg,webp}")
    .pipe(imagemin())
    .pipe(dest("./dist/img/"))
    .pipe(bsServer.reload({ stream: true }));
}

function styles() {
  return src("./src/styles/style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCss())
    .pipe(rename({ basename: "style.min", extname: ".css" }))
    .pipe(dest("./dist/css/"))
    .pipe(bsServer.reload({ stream: true }));
}

function scripts() {
  return src("./src/js/index.js")
    .pipe(uglify())
    .pipe(rename({ basename: "index.min", extname: ".js" }))
    .pipe(dest("./dist/js/"))
    .pipe(bsServer.reload({ stream: true }));
}

function watcher() {
  watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}", images);
  watch("./src/styles/*.scss", styles);
  watch("./src/js/index.js", scripts);
  watch("./index.html").on("change", bsServer.reload);
}

export const dev = series(images, styles, scripts, parallel(serve, watcher));

export const build = series(clear, images, styles, scripts);

