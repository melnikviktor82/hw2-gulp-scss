

const headerMainBtn = document.querySelector('.header-main__btn');


headerMainBtn.addEventListener('click', () => {
    const openBtn = document.querySelector('.header-main__btn-burger');
    openBtn.classList.toggle('header-main__btn-burger--hidden');
    const closeBtn = document.querySelector('.header-main__btn-cross');
    closeBtn.classList.toggle('header-main__btn-cross--hidden');

    const menuList = document.querySelector('.header-main__nav-list');

    if (openBtn.classList.contains('header-main__btn-burger--hidden')) {
        menuList.classList.add('header-main__nav-list--active')
    } else {
        menuList.classList.remove('header-main__nav-list--active')
    }
})

